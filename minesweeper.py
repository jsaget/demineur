"""
Partie frontale de notre démineur
"""
from tkinter import HORIZONTAL, Button, E, N, S, StringVar, Tk, W, Widget, ttk
from typing import Callable, List, TypedDict

import minesweeperlib as mslib

COLORCODE = TypedDict('COLORCODE', {'text': str, 'fg': str, 'bg': str})

HAUTEUR = mslib.HAUTEUR
LARGEUR = mslib.LARGEUR
N_MINES = mslib.N_MINES

SYM_UNKNOWN = "?"
SYM_BOMB = "💣"

WIN = "Gagné !"
LOST = "Perdu..."
ONGOING = "Partie en cours"
NEW = "Nouvelle partie"


class CellButton:
    """
    Case frontale du démineur : cliquable et avec un affichage
    """

    i: int
    j: int
    backend_cell: mslib.Cell

    def reveal(self, grid: mslib.Grid, i: int, j: int,
               hook: Callable[[int], None]) -> None:
        """
        Effectue le résultat d'un clic sur la case (i, j):
        - Clique sur le backend
        - Met à jour l'affichage (grâce à la fonction hook)
        """
        game_status: int = grid.click(i, j)
        hook(game_status)

    def __init__(self, frame, grid, posi: int, posj: int,
                 reveal_hook: Callable[[int], None]) -> None:
        self.i = posi
        self.j = posj
        self.backend_cell = grid.get_cell(self.i, self.j)
        self.button = Button(
            frame,
            command=lambda:self.reveal(grid, self.i, self.j, reveal_hook),
            width=4, height=2
        )
        self.update()
        self.button.grid(column=posj, row=posi)

    def update(self) -> None:
        """
        Met à jour le bouton
        """
        color_code = self.colorcode()
        self.button["text"] = color_code["text"]
        self.button["foreground"] = color_code["fg"]
        self.button["background"] = color_code["bg"]

    def colorcode(self) -> COLORCODE:
        col: COLORCODE = {'text': str(self), 'fg': 'black', 'bg': 'light grey'}
        back_cell = self.backend_cell
        if back_cell.visible:
            n: int = back_cell.mined_neighbors
            if back_cell.mine:
                col['bg'] = 'red'
            elif n == 0:
                col['fg'] = "light grey"
            elif n == 1:
                col['fg'] = "blue"
            elif n == 2:
                col['fg'] = "green"
            elif n == 3:
                col['fg'] = "red"
            elif n == 4:
                col['fg'] = "dark blue"
            elif n == 5:
                col['fg'] = "brown"
            elif n == 6:
                col['fg'] = "cyan"
            elif n == 7:
                col['fg'] = "black"
            else:
                col['fg'] = "grey"
        return col

    def __str__(self):
        back_cell = self.backend_cell
        if not back_cell.visible:
            return SYM_UNKNOWN
        if back_cell.mine:
            return SYM_BOMB
        return str(back_cell.mined_neighbors)


class GridButton:
    height: int
    width: int
    cells: List[List[CellButton]]

    def __init__(self, frame: ttk.Frame, grid: mslib.Grid,
                 hook: Callable[[int], None]):
        self.height = grid.height
        self.width = grid.height
        cells: List[List[CellButton]] = []
        for i in range(self.height):
            cells.append([])
            for j in range(self.width):
                cells[-1].append(CellButton(frame, grid, i, j, hook))
        self.cells = cells


class MineSweeper:
    """
    La classe principale de notre application
    """

    status: StringVar  # Gagné ou Perdu ou En cours ou Nouvelle partie
    n_safe: StringVar  # Nombre de cases sûres invisibles
    grid: mslib.Grid  # Grille sous-jacente
    button_grid: GridButton  # Grille de boutons

    def update_n_safe(self) -> None:
        """
        Met à jour la valeur de n_safe pour qu'elle corresponde a la valeur
        de grid.n_invisible_safe_cells
        """
        n: int = self.grid.n_invisible_safe_cells
        strn: str = "Nombre de cases sûres restantes : {}".format(n)
        self.n_safe.set(strn)

    def after_click(self, game_status: int) -> None:
        """
        Hook appelé après avoir révélé une cellule qui révèle les autres
        cellules et détermine si la partie est finie
        """
        for cell_row in self.button_grid.cells:
            for cell in cell_row:
                cell.update()
        self.update_n_safe()
        if game_status > 0:
            self.status.set(WIN)
        elif game_status < 0:
            self.status.set(LOST)
        else:
            self.status.set(ONGOING)

    def __init__(self, root: Tk) -> None:

        root.title("Démineur")

        mainframe = ttk.Frame(root)
        mainframe.grid(column=0, row=0, sticky=N+W+E+S)

        # Partie inférieure : Grille
        self.grid = mslib.Grid()
        grid_frame = ttk.Frame(mainframe)
        self.button_grid = GridButton(grid_frame, self.grid, self.after_click)
        grid_frame.grid(column=0, row=1, sticky=N)

        # Partie supérieure : État de la partie
        status_frame = ttk.Frame(mainframe)
        self.status = StringVar()
        self.status.set(NEW)
        ttk.Label(status_frame, textvariable=self.status).grid(column=0, row=0,
                                                               stick=W)

        self.n_safe = StringVar()
        self.update_n_safe()
        ttk.Label(status_frame, textvariable=self.n_safe).grid(column=1, row=0,
                                                               sticky=E)

        status_frame.grid(column=0, row=0, sticky=N+E+W)

        # Rendu du tout
        child: Widget
        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)
        ttk.Separator(mainframe, orient=HORIZONTAL)
        mainframe.pack()


def main():
    root = Tk()
    MineSweeper(root)
    root.mainloop()


if __name__ == "__main__":
    main()
