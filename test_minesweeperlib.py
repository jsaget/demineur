"""
Module de test pour minesweeperlib
"""

import pytest

from minesweeperlib import Grid


def test_init_grid():
    with pytest.raises(AssertionError):
        Grid(0, 20, 10)
    with pytest.raises(AssertionError):
        Grid(20, 0, 10)
    with pytest.raises(AssertionError):
        Grid(20, 20, 0)
    with pytest.raises(AssertionError):
        Grid(20, 20, 400)


def test_click():
    my_grid = Grid(10, 10, 20)
    assert my_grid.click(4, 2) >= 0
    assert my_grid.click(5, 8) == my_grid.click(5, 8)
    my_grid_2 = Grid(10, 10, 99)
    assert my_grid_2.click(0, 0) > 0
    assert my_grid_2.click(1, 1) < 0


def test_get_cell():
    my_grid = Grid(10, 10, 20)
    for i in range(10):
        for j in range(10):
            assert my_grid.get_cell(i, j) == my_grid._content[i, j]
