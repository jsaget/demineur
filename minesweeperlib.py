"""
Implémentation du jeu du démineur (minesweeper).
Logique du jeu décrite dans la documentation de la classe Grid.
"""

from typing import Callable, List, Tuple, TypeVar

import numpy as np
import numpy.random as npr

# import numpy.typing as npt  NOTE: pas encore supporté

Alpha = TypeVar("Alpha")

HAUTEUR = 10
LARGEUR = 10
N_MINES = 20


def count_neighbors(
        prop: Callable[[Alpha], bool],
        table,  # : npt.NDArray[Alpha], NOTE: pas encore supporté
        i_0: int,
        j_0: int
) -> int:
    """Compte le nombre de voisins de table[i_0, j_0] vérifiant la propriété"""

    height: int
    width: int
    (height, width) = table.shape
    assert 0 <= i_0 < height
    assert 0 <= j_0 < width

    count: int = 0
    for i in range(max(0, i_0 - 1), min(height, i_0 + 2)):
        for j in range(max(0, j_0 - 1), min(width, j_0 + 2)):
            if (i, j) != (i_0, j_0):
                if prop(table[i, j]):
                    count += 1
    return count


class Cell:
    """
    Case d'une grille de démineur

    Pour une case ma_case :
    - ma_case.visible: bool est True ssi la case est révélée
    - ma_case.mine: bool est True ssi la case est une mine
    - ma_case.mined_neighbors: int contient le nombre de voisins de ma_case qui
    sont des mines
    """

    visible: bool
    mine: bool
    mined_neighbors: int

    def __init__(self, mine: bool = False, mined_neighbors: int = 0):
        self.visible = False
        self.mine = mine
        self.mined_neighbors = mined_neighbors

    def __str__(self):
        if self.mine:
            return "💣"
        return str(self.mined_neighbors)


class Grid():
    """
    Grille de démineur.


    Pour initialiser la grille :

    Grid(height = HAUTEUR, width = LARGEUR, n_mines = N_MINES) renvoie une
    grille où
    - height est la hauteur de la grille.
      height <= 0 lève AssertionError
    - width est la largeur de la grille
      width <= 0 lève AssertionError
    - n_mines est le nombre de mines à générer
      n_mines <= 0 ou n_mines >= height * width lèvent AssertionError


    Pour jouer un coup sur la grille ma_grille :

    ma_grille.click(i, j) simule un clic sur la case (i, j) de ma_grille,
    i.e. :
    - Si la case n'est pas visible, elle est révélée
        - Si une case révélée a un nombre de voisins minés nul, tous ses
          voisins se font révéler (et ainsi de suite)
    - Si la case contient une bombe, on renvoie un entier négatif
    - Si toutes les cases sans bombe sont révélées, on renvoie un entier
      positif
    - Sinon, on renvoie 0


    Pour accéder à une case donnée :

    ma_grille.get_cell(i, j) renvoie la case en position (i, j)


    Pour connaître les informations associées à une case :
    voir la classe Cell
    """

    height: int
    width: int
    # _content: npt.NDArray[Cell]  # NOTE: pas encore supporté
    n_mines: int
    n_invisible_safe_cells: int
    _first_click: bool

    def _update_neighbors(self):
        """
        Met à jour la valeur de mined_neighbors dans toute la grille
        """
        i: int
        j: int
        prop: Callable[[Cell], bool] = lambda x: x.mine
        for i in range(self.height):
            for j in range(self.width):
                cur_cell: Cell = self.get_cell(i, j)
                cur_cell.mined_neighbors = count_neighbors(prop, self._content, i, j)

    def __init__(
        self,
        height: int = HAUTEUR,
        width: int = LARGEUR,
        n_mines: int = N_MINES,
    ):

        # Vérifications
        assert height > 0
        assert width > 0
        assert height * width > n_mines > 0

        # Initialisation
        self.height = height
        self.width = width
        self.n_mines = n_mines
        self.n_invisible_safe_cells = height * width - n_mines
        self._first_click = False

        # Création de la grille
        pseudo_content: List[Cell] = [
            Cell(mine=True) for _ in range(self.n_mines)
        ] + [
            Cell() for _ in range(self.n_invisible_safe_cells)
        ]
        # content: npt.NDArray[Cell] # NOTE: pas encore supporté
        pseudo_content = npr.permutation(pseudo_content)
        content = np.array(pseudo_content).reshape((height, width))

        # Mise à jour des informations de la grille
        self._content = content
        self._update_neighbors()

    def get_cell(self, i: int, j: int):
        """
        Renvoie la cellule identifiée par self._content[i, j]
        """
        return self._content[i, j]

    def reveal(self, i: int, j: int) -> bool:
        """
        Révèle le contenu de la case self._content[i, j].
        Renvoie False si la case était déjà visible, True sinon.
        Propage la révélation si la case était invisible et si la case a 0
        voisins minés.
        """

        cur_cell: Cell = self._content[i, j]

        if cur_cell.visible:
            return False
        cur_cell.visible = True

        if not cur_cell.mine:
            self.n_invisible_safe_cells -= 1

        if cur_cell.mined_neighbors == 0:
            i_neigh: int
            j_neigh: int
            for i_neigh in range(max(0, i - 1), min(self.height, i + 2)):
                for j_neigh in range(max(0, j - 1), min(self.width, j + 2)):
                    self.reveal(i_neigh, j_neigh)

        return True

    def _find_one_safe(self) -> Tuple[int, int]:
        """
        Renvoie l'indice d'une case sûre dans la grille
        """
        i: int
        j: int
        lst: List[int] = []
        for i in range(self.height):
            for j in range(self.width):
                if not self.get_cell(i, j).mine:
                    lst.append(self.width * i + j)
        index = npr.choice(lst)
        return (index // self.width, index % self.width)

    def click(self, i: int, j: int) -> int:
        """
        Simule un clic sur la case self._content[i, j].
        Met à jour la grille en révélant la case [i, j].
        Renvoie un entier négatif si la partie est perdue, un entier positif si
        la partie est gagnée et 0 sinon.
        """
        if not self._first_click:
            self._first_click = True
            if self.get_cell(i, j).mine:
                i_safe: int
                j_safe: int
                (i_safe, j_safe) = self._find_one_safe()
                self.get_cell(i_safe, j_safe).mine = self.get_cell(i, j).mine
                self.get_cell(i, j).mine = False
                self._update_neighbors()
            while self._content[i, j].mine:
                self._content = npr.permutation(self._content)
        self.reveal(i, j)
        if self._content[i, j].mine:
            return -1
        if self.n_invisible_safe_cells == 0:
            return 1
        return 0

    def __eq__(self, other: object):
        if not isinstance(other, Grid):
            return NotImplemented
        s_cont = self._content
        o_cont = other._content
        if s_cont.shape == o_cont.shape:
            return (s_cont == o_cont).all
        return False

    def __str__(self) -> str:
        return str(self._content)
